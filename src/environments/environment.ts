// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  name: 'dev',
  production: false,
  display: {
    enabled: true,
    backgroundColor: '#acfca4'
  },
  api: {
    ivy: {
      url: 'http://hqdevbpaiiis01.test.local:8086',
      authorization: 'Basic SXZ5VXNlcjplZTg1MTA1Y2M2MTMxMjM5MzRhMmUwZWYzODBhODliNg=='
    }
  }
};
