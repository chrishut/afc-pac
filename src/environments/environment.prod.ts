export const environment = {
  name: 'prod',
  production: true,
  display: {
    enabled: true,
    backgroundColor: '#F32142'
  },
  api: {
    ivy: {
      url: 'http://ghpbpaiiis01:8086',
      authorization: 'Basic SXZ5VXNlcjpmYTQwOTkyOTM4ZDRmNGUxM2NhYjVjMzhkNDBhMjhjOA=='
    }
  }
};
