export const environment = {
  name: 'local',
  production: false,
  display: {
    enabled: true,
    backgroundColor: '#7facf4'
  },
  api: {
    ivy: {
      url: 'http://localhost:50448',
      authorization: 'Basic SXZ5VXNlcjplZTg1MTA1Y2M2MTMxMjM5MzRhMmUwZWYzODBhODliNg=='
    }
  }
};
