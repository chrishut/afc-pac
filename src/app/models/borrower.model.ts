import { IBorrower } from './borrower.interface';
import { PortalUserResult } from './portal-user-result.model';
export class Borrower implements IBorrower {

    id: number;
    firstName: string;
    lastName: string;
    email: string;
    dob: string;
    ssnHash: string;
    sms: boolean;

    // Is there a portal user with this borrower id?
    hasPortalAccount: boolean;

    // Is there a portal user with this borrower's DOB & SSNHash?
    hasUserAccount: boolean;

    hasDuplicateEmail: boolean;
    homePhone: string;
    workPhone: string;
    mobilePhone: string;
    fax: string;
    errors: string[];
    isValid: boolean;
    fileName: string;
    sort: number;

    constructor() {
        this.errors = new Array<string>();
    }

    validate(portalUserResult: PortalUserResult) {
        this.errors = new Array<string>();
        this.isValid = true;
        this.hasUserAccount = false;
        this.hasPortalAccount = false;

        if (portalUserResult) {
            this.hasUserAccount = true;

            // IE 11 compatibility fix
            // this.hasPortalAccount = portalUserResult.BorrowerIds.includes(this.id);
            this.hasPortalAccount = (portalUserResult.BorrowerIds.indexOf(this.id) > -1);
        }

        if (!this.id || this.id === 0) {
            this.errors.push('Missing borrower id');
            this.isValid = false;
        }

        if (!this.firstName || this.firstName.length === 0) {
            this.errors.push('Missing first name');
            this.isValid = false;
        }

        if (!this.lastName || this.lastName.length === 0) {
            this.errors.push('Missing last name');
            this.isValid = false;
        }

        if (!this.ssnHash || this.ssnHash.length === 0) {
            this.errors.push('Missing SSN');
            this.isValid = false;
        }

        if (!this.email || this.email.length === 0) {
            this.errors.push('Missing email');
            this.isValid = false;
        }

        const invalidEmailMsg = `The email address is not valid. Please correct the email address and try again.`;

        if (!this.validateEmail(this.email)) {
            this.errors.push(invalidEmailMsg);
            this.isValid = false;
        }

        if (!this.fileName || this.fileName.length === 0) {
            this.errors.push('Missing file name');
            this.isValid = false;
        }

        if (!this.dob || this.dob.length === 0) {
            this.errors.push('Missing date of birth');
            this.isValid = false;
        }

        const dupEmailMsg = `Each portal account on a loan must have a unique email address. ` +
            `To create a separate portal account for each borrower, please update the email for one of the borrowers. ` +
            `Note that married co-borrowers may share a single portal account if they choose to share a single email address.`;

        if (this.hasDuplicateEmail) {
            this.errors.push(dupEmailMsg);
            this.isValid = false;
        }

        if ((!this.homePhone || this.homePhone.length === 0) &&
            (!this.workPhone || this.workPhone.length === 0) &&
            (!this.mobilePhone || this.mobilePhone.length === 0) &&
            (!this.fax || this.fax.length === 0)) {
            this.errors.push('Must have at least one phone number');
            this.isValid = false;
        }

        const rDate = new Date(this.dob);
        const oldestDate = new Date('01/01/1000');

        if (oldestDate >= rDate) {
            this.errors.push('invalid date of birth');
            this.isValid = false;
        }
    }

    validateEmail(email: string): boolean {
        // tslint:disable-next-line:max-line-length
        const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        const result = regexp.test(email);

        if (!result) {
            console.log(result);
        }

        return result;
    }
}
