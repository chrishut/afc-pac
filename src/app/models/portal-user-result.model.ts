export class PortalUserResult {
  BorrowerIds: number[];
  SSNHash: string;
  DOB: Date;
  Created: Date;
  LastUpdate: Date;
}
