export interface IBorrower {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    dob: string;
    sms: boolean;
    ssnHash: string;
    hasPortalAccount: boolean;
    hasDuplicateEmail: boolean;
    homePhone: string;
    workPhone: string;
    mobilePhone: string;
    fax: string;
    errors: string[];
    isValid: boolean;
    fileName: string;
    sort: number;
}
