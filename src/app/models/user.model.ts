export class User {
    Borrowers: number[] = [];
    BorrowerId: number;
    consumerId: string;
    firstName: string;
    lastName: string;
    dob: string;
    email: string;
    ssnHash: string;
    sms: boolean;
    homePhone: string;
    workPhone: string;
    mobilePhone: string;
    fax: string;
    errors: string[] = [];

    fullName() {
        return this.lastName + ', ' + this.firstName;
    }

    isValid() {
        this.validate();
        return this.errors.length === 0;
    }

    validate() {
        this.errors = [];

        if (this.BorrowerId <= 0) {
            this.errors.push('invalid borrower id');
        }

        if (!this.firstName || this.firstName.length === 0) {
            this.errors.push('invalid first name');
        }

        if (!this.lastName || this.lastName.length === 0) {
            this.errors.push('invalid last name');
        }

        if (!this.ssnHash || this.ssnHash.length === 0) {
            this.errors.push('invalid ssn');
        }

        if (!this.dob || this.dob.length === 0) {
            this.errors.push('invalid date of birth');
        }

        if (!this.email || this.email.length === 0) {
            this.errors.push('invalid email');
        }

        if ((!this.homePhone || this.homePhone.length === 0) &&
            (!this.workPhone || this.workPhone.length === 0) &&
            (!this.mobilePhone || this.mobilePhone.length === 0) &&
            (!this.fax || this.fax.length === 0)) {
                this.errors.push('must have a valid phone number');
        }
    }
}
