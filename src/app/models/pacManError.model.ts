export class PacManError {
    FriendlyMessage: string;
    Message: string;
    StatusCode: number;
    StatusText: string;
    Url: string;
}
