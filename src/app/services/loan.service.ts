import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { HttpEvent } from '@angular/common/http/src/response';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import {map, tap, catchError} from 'rxjs/operators';
import { Borrower } from '../models/borrower.model';
import { User } from '../models/user.model';


import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { IBorrower } from '../models/borrower.interface';

@Injectable()
export class LoanService {

  private borrowersUrl = `${environment.api.ivy.url}/api/loans`;

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'authorization': environment.api.ivy.authorization,
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Origin': '*'
      })
  };

  // A method to get a list of borrowers for the provided id (Byte file data id).
  getBorrowers(fileDataId: number): Observable<IBorrower[]> {
    this.httpOptions.headers.append('Access-Control-Allow-Methods', 'GET');
    const url = `${this.borrowersUrl}/${fileDataId}/borrowers`;
    return this.http.get<IBorrower[]>(url, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Since PAC-MAN is an internal application, we want to present the user with detailed error information.
  // This information can also be logged to the error console for further examination.
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `A client-side error occurred: ${error.error.message}`;
      console.error(errorMessage);
    } else {
      let messageDetail = '';
      if (error.error.MessageDetail) {
        messageDetail = error.error.MessageDetail;
      } else {
        messageDetail = error.message;
      }
      errorMessage = `API server returnd status code: ${error.status}, ${error.statusText}. Error message: ${messageDetail}`;
      console.error(errorMessage);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(errorMessage);
  }
}
