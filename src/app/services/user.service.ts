import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, retry } from 'rxjs/operators';
import { map } from 'rxjs/operators/map';
import { HttpEvent } from '@angular/common/http/src/response';
import { User } from '../models/user.model';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { PacManError } from '../models/pacManerror.model';
import { PortalUserResult } from '../models/portal-user-result.model';

@Injectable()
export class UserService {

  readonly userExistsUrl = `${environment.api.ivy.url}/api/portal/users`;
  readonly findPortalUserUrl = `${environment.api.ivy.url}/api/portal/users/find`;
  readonly createUserUrl = `${environment.api.ivy.url}/api/portal/users`;
  readonly sendRegistrationEmailUrl = `${environment.api.ivy.url}/api/Portal/users/email/create`;

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json; charset=utf-8',
        'authorization': environment.api.ivy.authorization,
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Origin': '*'
      })
  };

  public create(user: User): Observable<any> {
    const url = this.createUserUrl;
    // this.httpOptions.headers.append('Access-Control-Allow-Methods', 'POST')
    return this.http.post(url, user, this.httpOptions);
  }

  public portalUserExists(borrowerId: number): Observable<any> {
    const url = `${this.userExistsUrl}/${borrowerId}/exists`;
    // this.httpOptions.headers.append('Access-Control-Allow-Methods', 'GET')
    return this.http.get<boolean>(url, this.httpOptions);
  }

  public findPortalUser(dob: string, ssnHash: string): Observable<PortalUserResult> {

    const httpParams = new HttpParams()
      .append('dob', dob)
      .append('ssnhash', this.pacmanUriEncoder(ssnHash));

    const options = {
      headers: this.httpOptions.headers,
      params: httpParams
    };

    const url = `${this.findPortalUserUrl}`;
    // const url = `${this.findPortalUserUrl}?dob=${dob}&ssnhash=${ssnHash}`;
    return this.http.get<any>(url, options);
  }

  public sendRegistrationEmail(borrowerId: number, sms: boolean): Observable<void | PacManError> {
    const url = `${this.sendRegistrationEmailUrl}/${borrowerId}?sms=${sms}`;
    // this.httpOptions.headers.append('Access-Control-Allow-Methods', 'GET')
    return this.http.post<any>(url, null, this.httpOptions)
      .pipe(
        catchError(err => this.handleHttpError(err))
      );
  }

  private handleHttpError(error: HttpErrorResponse): Observable<PacManError> {
    const pacManError = new PacManError();
    pacManError.FriendlyMessage = 'An error occured related to Portal user data. Retry your operation or contact support.';
    pacManError.Message = error.message;
    pacManError.StatusCode = error.status;
    pacManError.StatusText = error.statusText;
    pacManError.Url = error.url;
    return ErrorObservable.create(pacManError);
  }

  // A function to handle encoding of base 64 characters, + and /
  private pacmanUriEncoder(data: string): string {

    // Globally replace all "+" and "/" char.
    const tmp = data
      .replace(/\+/g, '%2B')
      .replace(/\//g, '%2F');

    return tmp;
  }

}
