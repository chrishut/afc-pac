import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoanService } from '../services/loan.service';
import { Borrower } from '../models/borrower.model';
import { Observable } from 'rxjs/Observable';
import { debounceTime } from 'rxjs/operators/debounceTime';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { PacManError } from '../models/pacManError.model';
import { PortalUserResult } from '../models/portal-user-result.model';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})

export class ListUsersComponent implements OnInit {
  dataReady: boolean; // spinner
  userCreate: boolean; // spinner
  byteFileId: number;
  fileName: string;
  hasBorrowerError = false;
  errorMessage = '';
  errorInfo = `You may return to BytePro to correct any errors and return to this windows to complete account creation. Simply click
  the Refresh button to validate the fixes.`;
  message = '';
  distinctEmails: Array<string> = [];
  public pacManError: PacManError;

  borrowers = new Array<Borrower>();
  users = new Array<User>();

  constructor(private userService: UserService, private loanService: LoanService, private route: ActivatedRoute) {
    this.dataReady = false;
    this.route.params.subscribe(res => this.byteFileId = res.id);
  }

  ngOnInit() {
    this.loadBorrowers(this.byteFileId);
  }

  refresh() {
    this.loadBorrowers(this.byteFileId);
  }

  sendRegistrationEmail(borId: number, sms: boolean): void {
    this.userService.sendRegistrationEmail(borId, sms).subscribe(
      (data: void) => {
        console.log(`Sent request for registration email.`);
      },
      (err: PacManError) => {
        this.pacManError = err;
        console.error(`Error when sending request for new registration email for borrower id: ${borId}`);
      },
      () => {
        this.message = 'Email has been sent.';
        console.log(`Completed sending registration email to borrower id`);
      });
  }

  public accountCreate(borrowerId: number): void {
    this.userCreate = true;

    // We do not want the user to click a borrower's create button twice.
    this.disableCreateButton(borrowerId);

    const user = this.transformBorrowerToUser(borrowerId);

    if (user) {
      this.userService.create(user).pipe(
        debounceTime(1000)
      ).subscribe(
        (data) => {
          // console.log(data);
          this.refresh();
        },
        err => {
          console.error(err);
        },
        () => {
          this.userCreate = false;
          // console.log('create request is complete');
        }
      );

    } else {
      this.errorMessage = 'Cannot locate borrower!';
    }
  }

  transformBorrowerToUser(borrowerId: number): User {
    const user = new User();
    const borrower = this.borrowers.find(x => x.id === borrowerId);
    user.BorrowerId = borrower.id;
    user.firstName = borrower.firstName;
    user.lastName = borrower.lastName;
    user.email = borrower.email;
    user.homePhone = borrower.homePhone;
    user.workPhone = borrower.workPhone;
    user.mobilePhone = borrower.mobilePhone;
    user.fax = borrower.fax;
    user.ssnHash = borrower.ssnHash;
    user.dob = borrower.dob;
    user.sms = borrower.sms;
    return user;
  }

  loadBorrowers(fileDataId: number) {

    // Initialize
    this.dataReady = false;
    this.hasBorrowerError = false;
    this.borrowers = new Array<Borrower>();
    this.pacManError = null;
    this.message = '';
    this.distinctEmails = [];

    this.loanService.getBorrowers(this.byteFileId)
      .subscribe(data => {
        if (data.length === 0) {
          this.errorMessage = 'Cannot find any borrowers for file data id: ' + this.byteFileId;
          return;
        }
        this.fileName = data[0].fileName;
        this.transform(data);
        this.dataReady = true;
      }, error => {
        this.errorMessage = `An error has occured while getting borrowers. ${error}.`;
      });
  }

  sortBorrowers(unsorted: Borrower[]): void {
    this.borrowers = unsorted.sort((leftSide, rightSide): number => {
      if (leftSide.sort < rightSide.sort) { return -1; }
      if (leftSide.sort > rightSide.sort) { return 1; }
      return 0;
    });
  }

  private transform(data: any): void {
    for (let i = 0; i < data.length; i++) {
      const d = data[i];
      const b = new Borrower();
      b.id = d.id;
      b.firstName = d.firstName;
      b.lastName = d.lastName;
      b.fileName = d.fileName;
      b.dob = d.dob;
      b.email = d.email;
      b.ssnHash = d.ssnHash;
      b.homePhone = d.homePhone;
      b.workPhone = d.workPhone;
      b.mobilePhone = d.mobilePhone;
      b.fax = d.fax;
      b.sort = d.sort;
      b.sms = d.sms;
      b.hasDuplicateEmail = this.checkDuplicateEmails(d.email);

      this.userService.findPortalUser(b.dob, b.ssnHash)
        .subscribe(
          portalUserResult => {
            b.validate(portalUserResult);

            if (b.errors.length > 0) {
              this.hasBorrowerError = true;
            }
          }, error => {
            this.errorMessage = <any>error;
          }, () => {
            this.borrowers.push(b);
            this.sortBorrowers(this.borrowers);
          });
    }
  }

  private checkDuplicateEmails(email: string): boolean {
    // Don't process null or empty emails fir duplicates.
    if (!email || email.length === 0) {
      return;
    }

    // IE BUG
    // Compare case insensitive
    // if (this.distinctEmails.includes(email.toLocaleLowerCase())) {
    //   return true;
    // }
    if (this.distinctEmails.indexOf(email.toLocaleLowerCase()) > -1) {
      return true;
    }

    // Add distinct email to collection, case insensitive
    this.distinctEmails.push(email.toLocaleLowerCase());
  }

  disableCreateButton(borrowerId: number): void {
    for (let i = 0; i < this.borrowers.length; i++) {
      const b = this.borrowers[i];
      if (b.id === borrowerId) {
        b.hasPortalAccount = true;
      }
    }
  }
}
