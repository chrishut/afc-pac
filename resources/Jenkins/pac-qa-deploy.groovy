#!groovy

// QA PAC-MAN DEPLOY Pipeline

def notifyStarted() {    
    mail to: notificationStarted,
        subject: "QA AFC PAC-MAN Deploy Started",
        body: "The QA AFC PAC-MAN Deploy build is starting ${env.BUILD_URL}"
}

def notifySuccess() {        
     mail to: notificationSuccess,
            subject: emailSubjectTemplateSuccess,
            body: "The QA AFC PAC-MAN deploy build has completed successfully ${env.BUILD_URL}"
}

pipeline {
    agent any
    
    stages {
        stage('Initialize'){
            steps {
                script{
                    notificationStarted = 'scott.mann@americanfinancing.net, lavanya.ediga@americanfinancing.net, kim.condill@americanfinancing.net, jennifer.morris@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationSuccess = 'scott.mann@americanfinancing.net, lavanya.ediga@americanfinancing.net, kim.condill@americanfinancing.net, jennifer.morris@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationFailure = 'scott.mann@americanfinancing.net, lavanya.ediga@americanfinancing.net, kim.condill@americanfinancing.net, jennifer.morris@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    emailSubjectTemplateSuccess ="QA AFC PAC-MAN Deploy Completed (SUCCESS): ${currentBuild.fullDisplayName}"
                    notifyStarted()
                }                
            }
        }

        stage('Source Code Checkout') {
            steps {                
                git branch: 'qa', credentialsId: '6ee3dbc1-4832-4d60-800e-2d8c50d9d90b', url: 'git@bitbucket.org:americanfinancing/afc-pac.git'                
            }
        }       

        stage('Restore packages') {
            steps {                                
                bat 'C:\\Progra~1\\nodejs\\npm install'               
            }                
        }

        stage('Build application') {
            steps {                       
                bat 'ng build -e qa'                
            }
        }

        stage('Remove existing QA PAC-MAN web application files') {
            steps {
                bat 'net use \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac Jump5{joy}!! /USER:afc\\svc_jenkins'                
                bat 'if exist \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac del \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac\\*  /F /S /Q'                
            }
        }

        stage('Copy build files to QA PAC-MAN web application') {
           steps {                       
               bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-qa-deploy\\dist \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac /Y /V'
               bat 'if not exist \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac\\assets mkdir \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac\\assets'                
               bat 'xcopy c:\\progra~2\\jenkins\\workspace\\pac-man-qa-deploy\\src\\assets \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac\\assets /E /Q '
               bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-qa-deploy\\src\\assets\\images\\favicon.ico \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac /Y /V'
               bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-qa-deploy\\resources\\iis\\web.config \\\\hqqabpaiiis01\\c$\\inetpub\\wwwroot\\afc-pac /Y /V'
           }
        }
    }
    post {
        success {
            script {
                currentBuild.result = 'SUCCESS'
                notifySuccess()
            }
        }        
        failure {
            script {
                currentBuild.result = 'FAILURE'
            }
        }
        always {
            step([$class: 'Mailer',
                notifyEveryUnstableBuild: true,
                recipients: notificationFailure,
                sendToIndividuals: true])
        }
    }    
}
   