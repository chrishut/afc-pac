#!groovy

// DEV PAC-MAN Build Pipeline

def notifyStarted() {    
    mail to: notificationStarted,
        subject: "DEV AFC PAC-MAN Build Started: ${currentBuild.fullDisplayName}",
        body: "The DEV AFC PAC-MAN build is starting ${env.BUILD_URL}"
}

def notifySuccess() {        
     mail to: notificationSuccess,
            subject: emailSubjectTemplateSuccess,
            body: "The DEV AFC PAC-MAN build has completed successfully ${env.BUILD_URL}"
}

pipeline {
    agent any
    
    stages {
        stage('Initialize'){
            steps {
                script{
                    notificationStarted = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    notificationSuccess = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationFailure = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    emailSubjectTemplateSuccess ="DEV AFC PAC-MAN Build Completed (SUCCESS): ${currentBuild.fullDisplayName}"
                    notifyStarted()
                }                
            }
        }

        stage('Source Code Checkout') {
            steps {                
                git branch: 'dev', credentialsId: '6ee3dbc1-4832-4d60-800e-2d8c50d9d90b', url: 'git@bitbucket.org:americanfinancing/afc-pac.git'                
            }
        }       

        stage('Restore packages') {
            steps {                                
                bat 'C:\\Progra~1\\nodejs\\npm install'               
            }                
        }

        stage('Build application') {
            steps {                       
                bat 'ng build'                
            }
        }        
    }
    post {
        success {
            script {
                currentBuild.result = 'SUCCESS'
                notifySuccess()
            }
        }        
        failure {
            script {
                currentBuild.result = 'FAILURE'
            }
        }
        always {
            step([$class: 'Mailer',
                notifyEveryUnstableBuild: true,
                recipients: notificationFailure,
                sendToIndividuals: true])
        }
    }    
}
   