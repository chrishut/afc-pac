#!groovy

// DEV PAC-MAN Deploy Pipeline

def notifyStarted() {    
    mail to: notificationStarted,
        subject: "DEV AFC PAC-MAN Deploy Started: ${currentBuild.fullDisplayName}",
        body: "The DEV AFC PAC-MAN deploy build is starting ${env.BUILD_URL}"
}

def notifySuccess() {        
     mail to: notificationSuccess,
            subject: emailSubjectTemplateSuccess,
            body: "The DEV AFC PAC-MAN deploy build has completed successfully ${env.BUILD_URL}"
}

pipeline {
    agent any
    
    stages {
        stage('Initialize'){
            steps {
                script{
                    notificationStarted = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationSuccess = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationFailure = 'scott.mann@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    emailSubjectTemplateSuccess ="DEV AFC PAC-MAN Deploy Completed (SUCCESS): ${currentBuild.fullDisplayName}"
                    notifyStarted()
                }                
            }
        }

        stage('Source Code Checkout') {
            steps {                
                git branch: 'dev', credentialsId: '6ee3dbc1-4832-4d60-800e-2d8c50d9d90b', url: 'git@bitbucket.org:americanfinancing/afc-pac.git'                
            }
        }       

        stage('Restore packages') {
            steps {                                
                bat 'C:\\Progra~1\\nodejs\\npm install'               
            }                
        }

        stage('Build application') {
            steps {                       
                bat 'ng build'                
            }
        }

        stage('Remove existing DEV PAC-MAN web application files') {
            steps {                
                bat 'del c:\\inetpub\\wwwroot\\afc-pac  /F /S /Q'
                //bat 'if exist c:\\inetpub\\wwwroot\\afc-pac\\assets rmdir c:\\inetpub\\wwwroot\\afc-pac\\assets /S /Q'                
            }
        }

        stage('Copy build files to DEV PAC-MAN web application') {
            steps {                       
                bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-dev-deploy\\dist c:\\inetpub\\wwwroot\\afc-pac /Y /V'
                bat 'if not exist c:\\inetpub\\wwwroot\\afc-pac\\assets mkdir c:\\inetpub\\wwwroot\\afc-pac\\assets'                
                bat 'xcopy c:\\progra~2\\jenkins\\workspace\\pac-man-dev-deploy\\src\\assets c:\\inetpub\\wwwroot\\afc-pac\\assets /E /Q '
                bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-dev-deploy\\src\\assets\\images\\favicon.ico c:\\inetpub\\wwwroot\\afc-pac /Y /V'
                bat 'copy c:\\progra~2\\jenkins\\workspace\\pac-man-dev-deploy\\resources\\iis\\web.config c:\\inetpub\\wwwroot\\afc-pac /Y /V'
            }
        }
    }
    post {
        success {
            script {
                currentBuild.result = 'SUCCESS'
                notifySuccess()
            }
        }        
        failure {
            script {
                currentBuild.result = 'FAILURE'
            }
        }
        always {
            step([$class: 'Mailer',
                notifyEveryUnstableBuild: true,
                recipients: notificationFailure,
                sendToIndividuals: true])
        }
    }    
}
   