#!groovy

// PROD PAC-MAN Build Pipeline (From Master branch)

def notifyStarted() {    
    mail to: notificationStarted,
        subject: "PROD AFC PAC-MAN Build Started: ${currentBuild.fullDisplayName}",
        body: "The PROD AFC PAC-MAN build is starting ${env.BUILD_URL}"
}

def notifySuccess() {        
     mail to: notificationSuccess,
            subject: emailSubjectTemplateSuccess,
            body: "The PROD AFC PAC build has completed successfully ${env.BUILD_URL}"
}

pipeline {
    agent any
    
    stages {
        stage('Initialize'){
            steps {
                script{
                    notificationStarted = 'scott.mann@americanfinancing.net, donavan.white@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    notificationSuccess = 'scott.mann@americanfinancing.net, donavan.white@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'                    
                    notificationFailure = 'scott.mann@americanfinancing.net, donavan.white@americanfinancing.net, richard.harper@americanfinancing.net, juan.morris@americanfinancing.net'
                    emailSubjectTemplateSuccess ="PROD AFC PAC-MAN Build Completed (SUCCESS): ${currentBuild.fullDisplayName}"
                    notifyStarted()
                }                
            }
        }

        stage('Source Code Checkout') {
            steps {                
                git branch: 'master', credentialsId: '6ee3dbc1-4832-4d60-800e-2d8c50d9d90b', url: 'git@bitbucket.org:americanfinancing/afc-pac.git'                
            }
        }       

        stage('Restore packages') {
            steps {                                
                bat 'C:\\Progra~1\\nodejs\\npm install'               
            }                
        }

        stage('Build application') {
            steps {                       
                bat 'ng build --target=production --environment=prod'               
            }
        }

          stage('Copy additional files to DIST folder') {
            steps {                                                       
                bat 'xcopy c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\src\\assets c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\dist\\assets /O /X /E /H /K /I /Q'
                bat 'xcopy c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\src\\assets\\images\\favicon.ico c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\dist\\ /Y /V /Q'         
                bat 'xcopy c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\resources\\iis\\web.config c:\\progra~2\\jenkins\\workspace\\pac-man-prod-build\\dist /E /Q'
            }
        }
    }
    post {
        success {
            script {
                currentBuild.result = 'SUCCESS'
                notifySuccess()
            }
        }        
        failure {
            script {
                currentBuild.result = 'FAILURE'
            }
        }
        always {
            step([$class: 'Mailer',
                notifyEveryUnstableBuild: true,
                recipients: notificationFailure,
                sendToIndividuals: true])
        }
    }    
}
   