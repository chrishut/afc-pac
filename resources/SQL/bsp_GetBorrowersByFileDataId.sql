USE [BytePro]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[bsp_GetBorrowersByFileDataID]
   @FileDataId int
AS
BEGIN
	
	select
		ROW_NUMBER() OVER(ORDER BY a.DisplayOrder, a.BorrowerID) AS Sort,
		f.FileDataId,
		f.FileName,
		b.BorrowerID,
		b.FirstName,
		b.LastName,
		b.DOB [DOB],
		ssn.HashedValue [SSNHash],
		b.Email,
		b.HomePhone,
		b.MobilePhone,		
		(select top 1 e.Phone [EmployerPhone]	from [dbo].[Employer] e with (nolock) where e.BorrowerID = b.borrowerId and e.FileDataID = @FileDataId and e.status=2) [EmployerPhone]
	from [dbo].[FileData] f with (nolock)	
	join [dbo].[Borrower] b with (nolock)
		on b.FileDataID = f.FileDataID
	join [dbo].AFC_ExtendedHashedValue ssn with (nolock)
	on ssn.FileDataId = f.FileDataID
	and ssn.BorrowerId = b.BorrowerID	
	join [dbo].[Application] a with (nolock)
		on a.FileDataID = f.FileDataID
		and ( a.BorrowerID = b.BorrowerID or a.CoBorrowerID = b.BorrowerID)	
	where f.FileDataID = @FileDataId
	order by a.DisplayOrder, a.BorrowerID	

END
go


















GO


