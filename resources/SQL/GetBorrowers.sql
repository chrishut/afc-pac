select 
	f.FileDataId,
	f.FileName,
	b.BorrowerID,
	b.FirstName,
	b.LastName,
	b.DOB,	
	ssn.HashedValue [SSNHash],
	b.Email,
	b.HomePhone,
	b.MobilePhone,
	b.Fax [FaxPhone],	
	a.ApplicationID,
	a.BorrowerID,
	a.CoBorrowerID,
	a.DisplayOrder
from [dbo].[FileData] f with (nolock)	
join [dbo].[Borrower] b with (nolock)
	on b.FileDataID = f.FileDataID
join [dbo].AFC_ExtendedHashedValue ssn with (nolock)
	on ssn.FileDataId = f.FileDataID
	and ssn.BorrowerId = b.BorrowerID
join [dbo].[Application] a with (nolock)
	on a.FileDataID = f.FileDataID
	and ( a.BorrowerID = b.BorrowerID or a.CoBorrowerID = b.BorrowerID)
where f.FileDataID = 2189513 -- 2189515 --2000019 --2069206 --2189515 --2189520
--where f.DateCreated > '2018-1-1'
order by a.DisplayOrder, a.BorrowerID

--update Borrower set firstname = 'Mara' where borrowerid = 1093706

/*
select 
	f.filename, 
	f.FileDataID,
	count(f.filename) 
from FileData f 
join borrower b
	on b.FileDataID = f.FileDataID
group by f.filename, f.filedataid 
having count(filename) > 2
order by count(f.filename) desc

*/
